Source: hfsplus
Section: otherosfs
Priority: optional
Maintainer: Debian QA Group <packages@qa.debian.org>
Build-Depends: dpkg-dev (>= 1.22.5), debhelper-compat (=13), docbook-to-man,
Standards-Version: 4.6.1
Vcs-Git: https://salsa.debian.org/debian/hfsplus.git
Vcs-Browser: https://salsa.debian.org/debian/hfsplus

Package: libhfsp0t64
Provides: ${t64:Provides}
Replaces: libhfsp0
Breaks: libhfsp0 (<< ${source:Version})
Architecture: linux-any
Section: libs
Depends: ${shlibs:Depends}, ${misc:Depends}
Description: Shared library to access HFS+ formatted volumes
 HFS+ is a modernized version of Apple Computers HFS Filesystem.
 Nowadays, it is widely used with more recent versions of MacOS.
 hfsplus consists of a library and a set of tools that allow access
 to HFS+ volumes.
 .
 This package contains a shared version of the library.

Package: hfsplus
Architecture: linux-any
Depends: ${shlibs:Depends}, ${misc:Depends}
Description: Tools to access HFS+ formatted volumes
 HFS+ is a modernized version of Apple Computers HFS Filesystem.
 Nowadays, it is widely used with more recent versions of MacOS.
 hfsplus consists of a library and a set of tools that allow access
 to HFS+ volumes.
 .
 This package contains the tools themselves.

Package: libhfsp-dev
Architecture: linux-any
Section: libdevel
Depends: libhfsp0t64 (= ${binary:Version}), libc6-dev, ${misc:Depends}
Description: Library to access HFS+ formatted volumes
 HFS+ is a modernized version of Apple Computers HFS Filesystem.
 Nowadays, it is widely used with more recent versions of MacOS.
 hfsplus consists of a library and a set of tools that allow access
 to HFS+ volumes.
 .
 This package contains everything you need to write programs that use
 this library, i.e. header files and an archive version of the library.
